#!/usr/bin/env python3
import os
import platform
import subprocess
import glob


_distro = platform.dist()
_PACKAGE_FILE = "packages_arch"


def main():
    # Read packages to install from the packages file
    packages = []
    package_errors = []

    with open(os.path.join(os.path.dirname(__file__), _PACKAGE_FILE), "r") as package_file:
        for lnum, line in enumerate(package_file.readlines()):
            line = line.strip("\n")
            if line and not line.startswith("#"):
                lineparts = line.split(" ")
                if len(lineparts) == 1:
                    subprocess.call(["sudo", "pacman", "-S", "--noconfirm", lineparts[0]])
                else:
                    package_errors.append((lnum, line))

    # Install AUR packages
    packages = []
    aur_errors = []

    with open(os.path.join(os.path.dirname(__file__), "packages_aur"), "r") as package_file:
        for lnum, line in enumerate(package_file.readlines()):
            line = line.strip("\n")
            if line and not line.startswith("#"):
                lineparts = line.split(" ")
                if len(lineparts) == 1:
                    subprocess.call(["pacaur", "-S", "−−noconfirm", lineparts[0]])
                else:
                    aur_errors.append((lnum, line))


    # Find and run the install scripts for all the dotfiles
    for installer in glob.glob('./*/install.sh'):
        subprocess.call(["chmod", "+x", installer])  # make executable
        subprocess.call([installer])

    # Find and run the install scripts for all the dotfiles
    for config in glob.glob('./*/config.sh'):
        subprocess.call(["chmod", "+x", config])  # make executable
        subprocess.call([config])

    # If there were errors in the packages file, output the lines here.
    if package_errors:
        print("\n{} error(s) in the packages file.".format(len(package_errors)))
        print("You should install these packages manually and fix the corresponding line.")
        print("The lines with errors:")
        for lnum, line in package_errors:
            print("{ln}: {l}".format(ln=lnum, l=line))

    if aur_errors:
        print("\n{} error(s) in the packages_aur file.".format(len(aur_errors)))
        print("You should install these packages manually and fix the corresponding line.")
        print("The lines with errors:")
        for lnum, line in aur_errors:
            print("{ln}: {l}".format(ln=lnum, l=line))


if __name__ == '__main__':
    main()
