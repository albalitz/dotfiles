#!/usr/bin/env bash
#
# Installs misc stuff.

echo
echo "#################################"
echo "#"
echo "# Installer: misc"
echo "#"
echo "#################################"

echo
echo "installing wallit..."
if [ -d $HOME/wallit ]; then
    echo
    echo "wallit is already installed. Moving on..."
else
    git clone https://github.com/albalitz/wallit.git ~/wallit
    cd "$HOME/wallit"
    pip3 install --user -r requirements.txt
    ln -s "$HOME/wallit/wallit.py" "$HOME/bin/wallit"
fi

echo
echo "installing mensa-cli..."
if [ -d $HOME/mensa-cli ]; then
    echo
    echo "mensa-cli is already installed. Moving on..."
else
    git clone https://github.com/albalitz/mensa-cli.git ~/mensa-cli
    cd "$HOME/mensa-cli"
    pip3 install --user -r requirements.txt
    ln -s "$HOME/mensa-cli/mensa.py" "$HOME/bin/mensa"
fi
