alias gs="clear && git status"
alias gd="git diff"
alias giff="git diff"

alias gps="git push"
alias gpl="git pull"

alias gl="git log --color --graph --pretty=medium"
alias gll="git log"
