#!/usr/bin/env bash
#
# Configure atom related stuff.

echo
echo "#################################"
echo "#"
echo "# Configuration: atom"
echo "#"
echo "#################################"

dotfiles_dir="${DOTFILES}/atom/.atom"
atom_directory="${HOME}/.atom"
files=$(ls -1 $dotfiles_dir)

echo
echo "Symlinking configuration files..."
for f in ${files[*]}; do
    source_file="${dotfiles_dir}/${f}"
    destination_file="${atom_directory}/${f}"
    echo "${source_file} -> ${destination_file}"
    ln -s "${source_file}" "${atom_directory}"
done

apm install --packages-file "${DOTFILES}/atom/installed-packages.txt"
