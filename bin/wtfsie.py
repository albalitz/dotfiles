#!/usr/bin/env python3
import sys
from random import SystemRandom


options = [
    "scrambled eggs",
    "ravioli",
    "pizza",
    "wraps",
    "spaghetti"
]

if len(sys.argv) > 1:
    if sys.argv[1].lower() in ["help", "h", "?", "wtf"]:
        print("Usage: wtfsie.py [meal [meal ...]]")

        quit()
    else:
        options = sys.argv[1:]

print("Why the fuck don't you eat {}?".format(SystemRandom().choice(options)))
