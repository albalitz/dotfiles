#!/usr/bin/env python3
"""slee.py

Calculate when to go to bed for ideal REM cycles.
No idea if this is correct. If not, please tell me.
"""
import argparse
from tzlocal import get_localzone

import dateparser
import pendulum


max_minutes = 60 * 8  # don't sleep more than this amount of hours
first_rem = 70  # first REM after 70 minutes
rem_interval = 90  # every following, 90 min
rem_length = 20  # may not be correct - wikipedia says starting with 15, later 25

debug = False


def dbg(msg):
    if debug:
        print("DEBUG", msg)


def parse_date(d):
    dt = dateparser.parse(d)
    if dt is None:
        raise AttributeError(f"Can't parse {args.wakeup}.")
    timestamp = dt.timestamp()
    return pendulum.from_timestamp(timestamp)


def calculate_duration(minutes):
    return minutes + rem_length + rem_interval


def max_rems_till_target(minutes):
    mins = first_rem
    num = 0
    while calculate_duration(mins) < minutes and calculate_duration(mins) < max_minutes:
        mins += rem_length
        mins += rem_interval
        num += 1
        dbg(f"Adding 1 REM cycle... Now at {num}.")
    return mins


def best_rem(target_time):
    parsed = parse_date(target_time)
    mins_till_target = parsed.diff().total_minutes()
    mins = max_rems_till_target(mins_till_target)
    time_for_bed = parsed.subtract(minutes=mins)
    return (mins, time_for_bed.in_timezone(get_localzone()))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = __doc__)

    parser.add_argument(
        "wakeup",
        action = "store",
        nargs = "?",
        default = "tomorrow 07:00",
        help = "The time you want to wake up. Uses `dateparser` to parse the date, thus supports multiple formats, e.g. 'tomorrow 07:00' (default) or '18.10.2017 07:00'"
    )

    parser.add_argument(
        "--debug",
        action = "store_true",
        help = "Allow debug output"
    )

    args = parser.parse_args()
    debug = args.debug

    dbg(args)

    sleep_duration, sleep_at = best_rem(args.wakeup)
    print("To wake up at", args.wakeup)
    print("Go to bed at", sleep_at.to_formatted_date_string(), sleep_at.to_time_string(), f"({sleep_at.diff_for_humans()})")
    duration_hours = sleep_duration // 60
    duration_minutes = sleep_duration % 60
    print("Sleep duration:", f"{duration_hours}:{duration_minutes}h")
