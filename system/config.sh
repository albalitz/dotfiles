#!/usr/bin/env bash
#
# Configure system related stuff.

echo
echo "#################################"
echo "#"
echo "# Configuration: system"
echo "#"
echo "#################################"

function create_touchpad_config() {
    echo
    echo "Creating touchpad configuration file..."
    sudo mkdir -p /etc/X11/xorg.conf.d
    sudo cp ~/.dotfiles/system/touchpad /etc/X11/xorg.conf.d/10-synaptics.conf
}

echo -n "Create touchpad configuration? [Y/n] "
read -r CONTINUE
case "${CONTINUE}" in
    [yY])
        create_touchpad_config
        ;;
    '')
        create_touchpad_config
        ;;
    *)
        ;;
esac

while true; do
    echo
    echo "Configuring ufw... What device are you on?"
    echo "1: Desktop"
    echo "2: Laptop"
    echo -n "> "
    read -r CHOICE
    case "${CHOICE}" in
        1)
            sudo ~/.dotfiles/system/ufw-desktop.sh
            break
            ;;
        2)
            sudo ~/.dotfiles/system/ufw-laptop.sh
            break
            ;;
        *)
            echo "wtf. Just pick a number."
            ;;
    esac
done

echo
echo "Creating home structure..."
mkdir ~/code
mkdir ~/uni
