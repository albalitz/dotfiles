# Allow using aliases with sudo
alias sudo="sudo "

# combine clear with ls
alias cl="clear && ls"
alias cll="clear && ls -lh"
alias cla="clear && ls -lAh"
alias clar="clear && ls -lAhR"

alias mkdir="mkdir -p"

# see and search running processes
alias psa="ps -fe"
alias psg="ps -fe | grep"

# Make file executable
alias chx="chmod +x"

alias c="cd ~/code"
