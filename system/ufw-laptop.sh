#!/usr/bin/env bash

if [[ "$(whoami)" != "root" ]];then
    echo "RUN $0 AS ROOT!"
    echo "press enter to aknowledge and continue or cancel with Ctrl-C"
    read -r
    exit -1
fi

# https://wiki.archlinux.org/index.php/OpenVPN#Prevent_leaks_if_VPN_goes_down

ufw reset

# Default policies
ufw default deny incoming
ufw default deny outgoing

# Openvpn interface
ufw allow in on tun0
ufw allow out on tun0
ufw allow in on tun1
ufw allow out on tun1


# Local Networks
ufw allow in on enp0s25 from 192.168.2.0/24
ufw allow out on enp0s25 to 192.168.2.0/24
ufw allow in on wlp2s0 from 192.168.2.0/24
ufw allow out on wlp2s0 to 192.168.2.0/24
ufw allow in on wlp0s20u2 from 192.168.2.0/24
ufw allow out on wlp0s20u2 to 192.168.2.0/24


# Openvpn
ufw allow out on enp0s25 to any port 1194
ufw allow in on enp0s25 from any port 1194
ufw allow out on wlp2s0 to any port 1194
ufw allow in on wlp2s0 from any port 1194
ufw allow out on wlp0s20u2 to any port 1194
ufw allow in on wlp0s20u2 from any port 1194

ufw allow out on enp0s25 to any port 53
ufw allow in on enp0s25 from any port 53
ufw allow out on wlp2s0 to any port 53
ufw allow in on wlp2s0 from any port 53
ufw allow out on wlp0s20u2 to any port 53
ufw allow in on wlp0s20u2 from any port 53


ufw enable
ufw status

sudo systemctl enable ufw.service
