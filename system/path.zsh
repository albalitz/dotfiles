# add $DOTFILES/bin to path
export PATH="$DOTFILES/bin:$PATH"

# add home binary folder to path
export PATH="$HOME/bin:$PATH"

export CODE=$HOME/code
export UNI=$HOME/uni
