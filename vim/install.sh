#!/usr/bin/env bash
#
# Install vim.

echo
echo "#################################"
echo "#"
echo "# Installer: vim"
echo "#"
echo "#################################"

echo
echo "Installing Vundle..."
# https://github.com/VundleVim/Vundle.vim
vundle_file="~/.vim/bundle/Vundle.vim"
if [ -f "$vundle_file" ]; then
    echo
    echo "Vundle is already installed."
else
    git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi
