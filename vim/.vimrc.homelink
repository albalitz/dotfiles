" some of the configurations are based on
" https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/
" vundle configuration
set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
Plugin 'scrooloose/syntastic'
Plugin 'flazz/vim-colorschemes'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'davidhalter/jedi-vim'
Plugin 'roxma/python-support.nvim'
Plugin 'rust-lang/rust.vim'
Plugin 'iamcco/markdown-preview.vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'leafgarland/typescript-vim'
Plugin 'editorconfig/editorconfig-vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" /vundle


set encoding=utf-8
set number
syntax on
set noswapfile
set noshowmode  " disable "-- INSERT --" in command line in favor of airline and to allow displaying python signatures there
colorscheme Monokai

set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent

set splitbelow
set splitright

set clipboard=unnamedplus
set guicursor=n:hor10,i:ver1,r:block

let mapleader = "-"

" automatically remove trailing whitespace
autocmd BufWritePre *.{c,cpp,*css,hs,html,java,js,py,rb,rs,tex,ts,ya?ml,*rc,*sh} %s/\s\+$//e

" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

nnoremap <leader>h :sp<CR>
nnoremap <leader>v :vsp<CR>

nnoremap <C-S> :NERDTreeToggle<CR>

" better buffer changing
nnoremap <C-B> :b#<CR>
nnoremap <C-N> :bn<CR>
nnoremap <C-P> :bp<CR>

" filetype based indentation
autocmd FileType yaml setlocal tabstop=2 softtabstop=2 shiftwidth=2

" terminal emulator
if has('nvim')
    tnoremap <Esc> <C-\><C-n>
endif

" NERDTree config
" ignore files in NERDTree
let NERDTreeIgnore=['\.pyc$', '\~$']
let NERDTreeShowHidden=1

" open NERDTree automatically when opening a dir with vim
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

" close vim if the only open window is NERDTree
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


" multicursor
let g:multi_cursor_use_default_mapping = 0
let g:multi_cursor_next_key='<C-E>'
let g:multi_cursor_prev_key='<C-Q>'
let g:multi_cursor_skip_key='<C-X>'
let g:multi_cursor_quit_key='<Esc>'


" vim-markdown config
let g:vim_markdown_folding_disabled = 1

" markdown-preview config
let g:mkdp_path_to_chrome = "firefox"
nnoremap <C-O> :MarkdownPreview<CR>

" python stuff
let g:jedi#show_call_signatures = 2
let g:jedi#popup_on_dot = 0

" rust stuff
let g:rustfmt_autosave = 1  " autoformat on save

" airline config
let g:airline#extensions#tabline#enabled = 1
set laststatus=2
let g:airline_theme='powerlineish'
