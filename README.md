dotfiles
========
This is a collection of my dotfiles, based on [mspl13's dotfiles](https://github.com/mspl13/dotfiles/).  
Also, since mspl13's dotfiles are in turn based on [holman's dotfiles](https://github.com/holman/dotfiles),
check out their dotfiles too. ;)

Hint: these dotfiles are made for Arch Linux.

## install
```shell
git clone https://github.com/albalitz/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
./install.sh
```

## packages
To configure which packages to install, check the description in the `packages_*` files.


## topics
These dotfiles are built around topics.
If you want to add a new topic, simply add a corresponding directory and put files concerning that area there.

`*.zsh` files are automatically included in zsh.
`*.homelink` files are automatically symlinked to your home directory without the .homelink extension.
Every `install.sh` and `config.sh` files are automatically run during the installation.
