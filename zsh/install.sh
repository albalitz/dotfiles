#!/usr/bin/env bash
#
# Install zsh related stuff.

echo
echo "#################################"
echo "#"
echo "# Installer: zsh"
echo "#"
echo "#################################"


echo
echo "Installing oh-my-zsh..."
if [[ -d "$HOME/.oh-my-zsh" ]]; then
    echo "oh-my-zsh is already installed. Moving on..."
else
    # installing oh-my-zsh according to https://github.com/robbyrussell/oh-my-zsh/
    cd "$HOME/Downloads"
    sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

    # Remove ~/.zshrc so the new one can be linked to the home diectory.
    rm ~/.zshrc
fi

echo
echo "Installing themes..."
mkdir -p "$HOME/.oh-my-zsh/custom/themes"
curl https://raw.githubusercontent.com/caiogondim/bullet-train-oh-my-zsh-theme/master/bullet-train.zsh-theme > "$HOME/.oh-my-zsh/custom/themes/bullet-train.zsh-theme"
echo "done."
