# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
ZSH_THEME="bullet-train"

# bullet-train theme config
BULLETTRAIN_PROMPT_ORDER=(
    status
    custom
    context
    dir
    screen
    git
    hg
    cmd_exec_time
)
BULLETTRAIN_STATUS_EXIT_SHOW=true
BULLETTRAIN_PROMPT_ADD_NEWLINE=false

fpath=($DOTFILES/functions $fpath)
autoload -U $DOTFILES/functions/*(:t)

plugins=()

# history settings
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
HIST_STAMPS="yyyy-mm-dd"
