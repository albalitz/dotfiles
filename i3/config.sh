#!/usr/bin/env bash
#
# Configure i3 related stuff.

echo
echo "#################################"
echo "#"
echo "# Configuration: i3"
echo "#"
echo "#################################"

echo
echo "Configuring i3..."
mkdir -p ~/.config/i3
ln -s ~/.dotfiles/i3/i3_config ~/.config/i3/config

echo
echo "Symlinking gtk3 config..."
ln -s $DOTFILES/i3/gtk3_settings.ini $HOME/.config/gtk-3.0/settings.ini
