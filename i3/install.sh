#!/usr/bin/env bash
#
# Install the i3 window manager.

echo
echo "#################################"
echo "#"
echo "# Installer: i3"
echo "#"
echo "#################################"

echo
echo "Installing i3..."
sudo pacman -S --noconfirm i3 i3blocks dmenu
pacaur -S --noconfirm xinit-xsession

echo
echo "Installing a display manager..."
sudo pacman -S --noconfirm lightdm
sudo systemctl enable lightdm.service

echo
echo "Installing and configuring a greeter..."
# When changing the installed greeter, also adapt the changes in the lightdm.conf

pacaur -S --noconfirm lightdm-pantheon-greeter

sudo ln -s ~/.dotfiles/i3/lightdm.conf /etc/lightdm/lightdm.conf

echo
echo "Go to"
echo "https://wiki.archlinux.org/index.php/xorg#Driver_installation"
echo "and install the driver you need for this system."
echo "Press [ENTER] to continue..."
read
