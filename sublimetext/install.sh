#!/usr/bin/env bash
#
# Install sublime text related stuff.

echo
echo "#################################"
echo "#"
echo "# Installer: sublime text"
echo "#"
echo "#################################"

echo
echo "Downloading and installing Sublime Text Package Control ..."
package_control="https://packagecontrol.io/Package%20Control.sublime-package"
package_control_dir="$HOME/.config/sublime-text-3/Installed Packages/Package Control.sublime-package"
if [ -f "$package_control_dir" ] || ! wget -q --spider $package_control; then
    echo
    echo "Package Control is already installed or the given link is not valid. Ignoring..."
else
    wget $package_control -O "$package_control_dir"
fi
