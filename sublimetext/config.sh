#!/usr/bin/env bash
#
# Configure sublime text related stuff.

echo
echo "#################################"
echo "#"
echo "# Configuration: sublime text"
echo "#"
echo "#################################"

sublime_directory="$HOME/.config/sublime-text-3"

echo
echo "Linking Sublime Text settings..."
if [[ -e "$sublime_directory/Packages/User/Preferences.sublime-settings" ]]; then
    mv "$sublime_directory/Packages/User/Preferences.sublime-settings" "$sublime_directory/Packages/User/Preferences.sublime-settings_before-dotfiles"
fi
ln -s "$HOME/.dotfiles/sublimetext/preferences" "$sublime_directory/Packages/User/Preferences.sublime-settings"

echo
echo "Linking Sublime Text keybindings..."
if [[ -e "$sublime_directory/Packages/User/Default (Linux).sublime-keymap" ]]; then
    mv "$sublime_directory/Packages/User/Default (Linux).sublime-keymap" "$sublime_directory/Packages/User/Default (Linux).sublime-keymap_before-dotfiles"
fi
ln -s "$HOME/.dotfiles/sublimetext/keybindings" "$sublime_directory/Packages/User/Default (Linux).sublime-keymap"

echo
echo "Linking Sublime Text Package Control..."
if [[ -e "$sublime_directory/Packages/User/Package Control.sublime-settings" ]]; then
    mv "$sublime_directory/Packages/User/Package Control.sublime-settings" "$sublime_directory/Packages/User/Package Control.sublime-settings_before-dotfiles"
fi
ln -s "$HOME/.dotfiles/sublimetext/package_control" "$sublime_directory/Packages/User/Package Control.sublime-settings"


echo
echo "Linking your syntax specific Sublime settings..."
for src in $(find -H "$DOTFILES/sublimetext/" -maxdepth 2 -name '*.sublime-settings' -not -path '*.git*')
do
    dst="$sublime_directory/Packages/User/$(basename $src)"
    if [[ -e "$dst" ]]; then
        mv $dst $dst"_before-dotfiles"
    fi

    ln -s "$src" "$dst"
done

echo
echo "Opening Sublime Text now to make sure the packages are installed..."
if [ "$(lsb_release -si)" == "Ubuntu" ]; then
    subl
elif [ "$(lsb_release -si)" == "Arch" ]; then
    subl3
fi
