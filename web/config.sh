#!/usr/bin/env bash
#
# Configure web stuff.

echo
echo "#################################"
echo "#"
echo "# Configuration: web"
echo "#"
echo "#################################"

echo
echo "Configuring qutebrowser..."
dotfiles_dir="${DOTFILES}/web/qutebrowser"
qutebrowser_directory="${HOME}/.config/qutebrowser"
files=$(ls -1 $dotfiles_dir)
for f in ${files[*]}; do
    source_file="${dotfiles_dir}/${f}"
    destination_file="${qutebrowser_directory}/${f}"
    echo "${source_file} -> ${destination_file}"
    ln -s "${source_file}" "${qutebrowser_directory}"
done
