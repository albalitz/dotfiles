#!/usr/bin/env bash
#
# Install telegram.

echo
echo "#################################"
echo "#"
echo "# Installer: telegram"
echo "#"
echo "#################################"

echo
echo "Installing telegram-cli..."
# https://github.com/vysheng/tg
pacaur -S telegram-cli-git

echo "Installing nctelegram..."
git clone https://github.com/Nanoseb/ncTelegram.git ~/ncTelegram
ln -s $HOME/ncTelegram/nctelegram $HOME/bin/tg

echo
echo "Starting telegram-cli so you can log in to your account."
telegram-cli

if [[ $? == 0 ]]; then
    echo "Telegram is now set up."
else
    echo "telegram-cli returned with a non 0 exit code."
    echo "Please set up telegram manually by running `telegram-cli`"
fi


echo
# see https://github.com/telegramdesktop/tdesktop/issues/960
# for why linking stuff
echo "Configuring telegram-desktop..."
mkdir "${HOME}/.TelegramDesktop"
ln -s $(which telegram-desktop) "${HOME}/.TelegramDesktop/Telegram"
