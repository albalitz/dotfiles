#!/usr/bin/env bash
#
# Configure dunst.

echo
echo "#################################"
echo "#"
echo "# Configuration: dunst"
echo "#"
echo "#################################"

echo
echo "Configuring dunst..."
mkdir -p ~/.config/dunst
ln -s ~/.dotfiles/dunst/dunstrc ~/.config/dunst/dunstrc
