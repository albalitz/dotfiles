#!/usr/bin/env bash
#
# Configure gpg related stuff.

echo
echo "#################################"
echo "#"
echo "# Configuration: gpg"
echo "#"
echo "#################################"

echo
echo "Configuring gpg..."

echo
echo "Symlinking gpg config..."
ln -s $DOTFILES/gpg/gpg-agent.conf $HOME/.gnupg/gpg-agent.conf
